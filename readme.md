# slue-fs
Provide one or a array list of glob, to read and write files by stream. 
## read
```javascript
const slueFs = require('slue-fs');
const transformObj = require('slue-stream').transformObj;
const patterns = ['./src/**/*.js'];

slueFs.read(patterns).pipe(transformObj(function(file, env, cb) {
    console.log(file);
    cb(null, file);
}));
```

## write
```javascript
const slueFs = require('slue-fs');
const transformObj = require('slue-stream').transformObj;
slueFs.read('./test/a.js').pipe(transformObj(function(obj, env, cb) {
    cb(null, obj);
})).pipe(slueFs.write('./test/park'));
```

## watch
```javascript
const slueFs = require('slue-fs');
let watcher = slueFs.watch(['./src/**/*.js']);
watcher.on('change', function(filePath, stat) {
    console.log(filePath);
});
```