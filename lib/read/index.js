const util = require('util');
const path = require('path');
const fs = require('fs');
const globs2 = require('globs2');
const File = require('vinyl');
const slueStream = require('slue-stream');

//const stripBOM = require('strip-bom');
// function stripBOM(content) {
//     if (content.charCodeAt(0) === 0xFEFF) {
//         content = content.slice(1);
//     }
//     return content;
// }

// function read(globs, options = {}) {
//     if (!isValidGlob(globs)) {
//         throw new Error('Invalid glob argument: ' + glob);
//     }

//     let stream = slueStream.readable();

//     if (util.isArray(globs)) {
//         if (glob.length === 0) {
//             process.nextTick(stream.end.bind(stream));
//             return stream;
//         }
//     } else {
//         globs = [globs];
//     }

//     let cwd = process.cwd();

//     let streams = globs.map(function(theGlob) {
//         let stream = slueStream.readable();

//         let globIndtance = new glob.Glob(theGlob);
        
//         globIndtance.on('error', stream.emit.bind(stream, 'error'));
//         globIndtance.on('end', function() {
//             //stream.end();
//             stream.push(null);
//         });

//         let matchedFiles = glob.sync(theGlob);
//         let basePath = glob2base(globIndtance);
//         matchedFiles.forEach(function(filename) {
//             let globFile = Object.assign({}, options);
//             globFile.cwd = cwd;
//             globFile.base = basePath;
//             globFile.path = path.resolve(cwd, filename);
//             globFile.contents = fs.readFileSync(globFile.path);
//             stream.push(globFile);
//         });

//         return stream;
//     });

//     let theStream = streams.length === 1 ? streams[0] : slueStream.combine(streams);
//     theStream.on('end', function() {
//         theStream.push(null);
//     });
//     return theStream.pipe(slueStream.transformObj(function(globFile, env, cb) {
//         try {
//             cb(null, new File(globFile));
//         } catch(e) {}
//     }))
// };
function read(patterns, options = {}) {
    if (!isValidGlob(patterns)) {
        throw new Error('Invalid glob argument');
    }

    let stream = slueStream.readable();

    if (Array.isArray(patterns)) {
        if (patterns.length === 0) {
            process.nextTick(stream.end.bind(stream));
            return stream;
        }
    } else {
        patterns = [patterns];
    }

    let cwd = process.cwd();

    let globsMatchRes = globs2.sync(patterns);
    globsMatchRes.forEach(function(res) {
        let globFile = Object.assign({}, options);
        globFile.cwd = cwd;
        globFile.base = res.base;
        globFile.path = path.resolve(cwd, res.file);
        globFile.contents = fs.readFileSync(globFile.path);
        stream.push(globFile);
    });

    stream.push(null);
    let theStream = slueStream.combine([stream]);
    theStream.on('end', function() {
        theStream.push(null);
    });
    return theStream.pipe(slueStream.transformObj(function(globFile, env, cb) {
        try {
            cb(null, new File(globFile));
        } catch(e) {
            console.log(e);
        }
    }))
};

module.exports = read;

function isValidGlob(glob) {
    if (typeof glob === 'string') {
        return true;
    }
    if (Array.isArray(glob) && glob.length !== 0) {
        return glob.every(isValidGlob);
    }
    if (Array.isArray(glob) && glob.length === 0) {
        return true;
    }
    return false;
}