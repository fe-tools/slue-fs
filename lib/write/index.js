const util = require('util');
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
const File = require('vinyl');
const slueStream = require('slue-stream');

function write(folder, opts = {}) {
  if (!util.isString(folder) && !util.isFunction(folder)) {
    throw new Error('Invalid output folder');
  }

  let cwd = process.cwd();
  let __fs = this.fileSystem || fs;

  function saveFS(file, env, cb) {
    let basePath = '';
    if (util.isString(folder)) {
      basePath = path.resolve(cwd, folder);
    }
    if (util.isFunction(folder)) {
      basePath = path.resolve(cwd, folder(file));
    }

    let filePath = path.resolve(basePath, file.relative);
    let fileFoler = path.dirname(filePath);

    file.cwd = cwd;
    file.base = basePath;
    file.path = filePath;

    mkdirp(fileFoler, function(err) {
      if (err) {
        return cb(err);
      }

      let filePathParser = path.parse(filePath);
      if (opts.ext) {
        filePathParser.ext = opts.ext;
      }
      if (opts.filename) {
        filePathParser.name = opts.filename;
      }
      filePathParser.base = `${filePathParser.name}${filePathParser.ext}`;

      __fs.writeFileSync(path.format(filePathParser), file.contents);

      cb(null, file);
    });
  }
  let stream = slueStream.transformObj(saveFS);
  return stream;
};

module.exports = write;